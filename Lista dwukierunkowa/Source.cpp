#include <iostream>
#include <string>
using namespace std;

struct Element
{
	Element* nastepny;
	Element* poprzedni;
	string rodzaj;
	string kolor;
};
class Lista
{
	Element* glowa;
	Element* ogon;
	int licznik;
public:

	Lista()
	{
		glowa = ogon = 0;
		licznik = 0;
	}
	~Lista()
	{
		Element * f;

		while (glowa)
		{
			f = glowa->nastepny;
			delete glowa;
			glowa = f;
		}
	}
	Element* push_front(Element* f)
	{
		f->nastepny = glowa;
		f->poprzedni = NULL;
		if (glowa) glowa->poprzedni = f;
		glowa = f;
		if (!ogon) ogon = glowa;
		licznik++;
		return glowa;
	}
	Element* push_back(Element* f)
	{
		if (ogon) ogon->nastepny = f;
		f->nastepny = NULL;
		f->poprzedni = ogon;
		ogon = f;
		if (!glowa) glowa = ogon;
		licznik++;
		return ogon;
	}
	Element* usun(Element* f)
	{
		if (f->poprzedni) f->poprzedni->nastepny = f->nastepny;
		else glowa = f->nastepny;
		if (f->nastepny) f->nastepny->poprzedni = f->poprzedni;
		else ogon = f->poprzedni;
		licznik--;
		return f;
	}
	void pokaz()
	{
		Element * f;

		if (!glowa) cout << "Lista jest pusta." << endl;
		else
		{
			f = glowa;
			while (f)
			{
				cout << f->rodzaj << " " << f->kolor << ", ";
				f = f->nastepny;
			}
			cout << endl;
		}
	}
};
int main()
{
	Lista z;
	Element * f = new Element;
	z.pokaz();
	delete f;
	system("pause");
	return 0;
}
